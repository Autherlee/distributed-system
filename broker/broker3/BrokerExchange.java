import java.io.*;
import java.net.*;

public class BrokerExchange {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
	Socket brokerSocket = null;
	ObjectOutputStream out = null;
	ObjectInputStream in = null;
	
	try {	
	    /* Default hostname and port */
	    String hostname = "localhost";
	    int port = 4444;
	    
	    if (args.length == 2){
		hostname = args[0];
		port = Integer.parseInt(args[1]);
	    } else {
		System.err.println("ERROR: Invalid arguments!");
		System.exit(-1);
	    }
	    brokerSocket = new Socket(hostname, port);
	    
	    out = new ObjectOutputStream(brokerSocket.getOutputStream());
	    in = new ObjectInputStream(brokerSocket.getInputStream());
	} catch (UnknownHostException e){
	    System.err.println("ERROR: Don't know where to connect!!");
	    System.exit(1);
	} catch (IOException e){
	    System.err.println("ERROR: Couldn't get I/O for the connection.");
	    System.exit(1);
	}
	
	BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in));
	
	String userInput;
	
	System.out.println("Enter Command or quit for exit:\n");
	System.out.println(">");
	
	while((userInput = stdIn.readLine()) != null && userInput.toLowerCase().indexOf("quit") == -1){
	    
	    /* Make a new request packet */
	    BrokerPacket packetToServer = new BrokerPacket();
	    
	    if(userInput.split(" ")[0].toLowerCase().indexOf("add")!=-1){
	    	
	    	    	
	    	  packetToServer.type = BrokerPacket.EXCHANGE_ADD;	    	
	    }
	    
	    
	    if(userInput.split(" ")[0].toLowerCase().indexOf("update")!=-1){
	    	
	    	    	
	    	  packetToServer.type = BrokerPacket.EXCHANGE_UPDATE;	    	
	    }

	    
	    if(userInput.split(" ")[0].toLowerCase().indexOf("remove")!=-1){
	    	
	    	    	
	    	  packetToServer.type = BrokerPacket.EXCHANGE_REMOVE;	    	
	    }
	    
	    
	  
	   packetToServer.symbol = userInput.toLowerCase();
	   // packetToServer.symbol = userInput;
	    
	    out.writeObject(packetToServer);
	    
	    /* Print server reply */
	    BrokerPacket packetFromServer;
	    packetFromServer = (BrokerPacket) in.readObject();
	    
	    if (packetToServer.type == BrokerPacket.EXCHANGE_ADD){
		
	    	System.out.println(packetFromServer.symbol);
	    	
	    }
	
	    if (packetToServer.type == BrokerPacket.EXCHANGE_REMOVE){
			
	    	
	    	System.out.println(packetFromServer.symbol);
		
	    }
	    
	    
	    if (packetToServer.type == BrokerPacket.EXCHANGE_UPDATE){
			System.out.println(packetFromServer.symbol);
		    }
	   
	    
	    /* Reprint the console prompt */
	    System.out.println(">");
	    
	}
	
	/* Tell server that I'm quitting */
	BrokerPacket packetToServer = new BrokerPacket();
	packetToServer.type = BrokerPacket.BROKER_BYE;
	packetToServer.symbol = "x";
	out.writeObject(packetToServer);
	
	out.close();
	in.close();
	stdIn.close();
	brokerSocket.close();
    }
}
