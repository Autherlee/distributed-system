import java.net.*;
import java.io.*;
import java.util.*;
import java.util.concurrent.Exchanger;

public class BrokerServerHandlerThread extends Thread {
    private Socket socket = null;
    private Map<String, String> nasdaq = new HashMap<String, String>();
    
    private String broker_name; 
    public BrokerServerHandlerThread(Socket socket, Map<String, String> nasdaq ){
	super("BrokerServerHandlerThread");
	this.socket = socket;
	this.nasdaq = nasdaq;
	
	
	
	
	
	System.out.println("Created new Thread to handle client");	
    }
    
    public void run() {
	
	boolean gotByePacket = false;
	
	try {
		
		//comunicate with another server 
		
		
		Socket brokerSocket = null;
		ObjectOutputStream out_as_client = null;
		ObjectInputStream in_as_client  = null;
		
		String hostname = "localhost";
	    int port = 7500;
	 
	    
	    brokerSocket = new Socket(hostname, port);
	    
	    out = new ObjectOutputStream(brokerSocket.getOutputStream());
	    in = new ObjectInputStream(brokerSocket.getInputStream());
		
		
		
		
		
		
		
	    /* stream to read from client */
	    ObjectInputStream fromClient = new ObjectInputStream(socket.getInputStream());
	    BrokerPacket packetFromClient;
	    
	    /* stream to write back to client */
	    ObjectOutputStream toClient = new ObjectOutputStream(socket.getOutputStream());
	    
	    
	    while (( packetFromClient = (BrokerPacket) fromClient.readObject()) != null) {
		/* create a packet to send reply back to client */
		BrokerPacket packetToClient = new BrokerPacket();
		packetToClient.type = BrokerPacket.BROKER_QUOTE;
		
		/* process message */
		if(packetFromClient.type == BrokerPacket.BROKER_REQUEST) {
		    
		    String quote = nasdaq.get(packetFromClient.symbol);

		    if (quote == null){
			
		    	
		    	
		    	
		    	
		    	
		    	
		    	
		    	
		    } else {
			packetToClient.quote = Long.parseLong(quote);
		    }
		    
		    /* send reply back to client */
		    toClient.writeObject(packetToClient);
		    
		    /* wait for next packet */
		    continue;
		}
		
		/*HANDLE EXCHANGE_ADD*/
		
		if(packetFromClient.type == BrokerPacket.EXCHANGE_ADD  ) {
			
			

			String[] lineToArray = packetFromClient.symbol.split(" ");
			    
			nasdaq.put(lineToArray[1],"10");
			
			packetToClient.symbol = lineToArray[1] + " added";
			
			
			toClient.writeObject(packetToClient);
			
			
			continue;
			
				
			
		}
		
		/*Handle exchange_update*/
			
		
		
		
		if(packetFromClient.type == BrokerPacket.EXCHANGE_UPDATE) {
			
			

			String[] lineToArray = packetFromClient.symbol.split(" ");
			
			String new_quote = nasdaq.remove(lineToArray[1]);
			
			nasdaq.put(lineToArray[1], lineToArray[2]) ;
			
			packetToClient.symbol = lineToArray[1] + " updated";
			
			
			toClient.writeObject(packetToClient);
			
			
			continue;
			
				
			
		}
		
		
		

		/*Handle exchange_remove*/
			
		
		
		
		if(packetFromClient.type == BrokerPacket.EXCHANGE_REMOVE) {
			
			

			String[] lineToArray = packetFromClient.symbol.split(" ");
			
			String new_quote = nasdaq.remove(lineToArray[1]);
			
			
			
			packetToClient.symbol = lineToArray[1] + " removed";
			
			
			toClient.writeObject(packetToClient);
			
			
			continue;
			
				
			
		}
		
		
		
		
		
		
		/* Sending an BROKER_NULL || BROKER_BYE means quit */
		if (packetFromClient.type == BrokerPacket.BROKER_NULL || packetFromClient.type == BrokerPacket.BROKER_BYE) {
		    gotByePacket = true;
		    packetToClient = new BrokerPacket();
		    packetToClient.type = BrokerPacket.BROKER_BYE;
		    toClient.writeObject(packetToClient);
		    break;
		}
		
		/* if code comes here, there is an error in the packet */
		System.err.println("ERROR: Unknown  BROKER packet!!");
		System.exit(-1);
	    }
	    
	    /* cleanup when client exits */
	    fromClient.close();
	    toClient.close();
	    socket.close();
	    
	} catch (IOException e) {
	    if(!gotByePacket)
		e.printStackTrace();
	} catch (ClassNotFoundException e) {
	    if(!gotByePacket)
		e.printStackTrace();
	}
    }
}
