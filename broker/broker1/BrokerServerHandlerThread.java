import java.net.*;
import java.io.*;
import java.util.*;
import java.util.concurrent.Exchanger;

public class BrokerServerHandlerThread extends Thread {
    private Socket socket = null;
    private Map<String, String> nasdaq = new HashMap<String, String>();
    
    public BrokerServerHandlerThread(Socket socket, Map<String, String> nasdaq){
	super("BrokerServerHandlerThread");
	this.socket = socket;
	this.nasdaq = nasdaq;
	System.out.println("Created new Thread to handle client");	
    }
    
    public void run() {
	
	boolean gotByePacket = false;
	
	try {
	    /* stream to read from client */
	    ObjectInputStream fromClient = new ObjectInputStream(socket.getInputStream());
	    BrokerPacket packetFromClient;
	    
	    /* stream to write back to client */
	    ObjectOutputStream toClient = new ObjectOutputStream(socket.getOutputStream());
	    
	    
	    while (( packetFromClient = (BrokerPacket) fromClient.readObject()) != null) {
		/* create a packet to send reply back to client */
		BrokerPacket packetToClient = new BrokerPacket();
		packetToClient.type = BrokerPacket.BROKER_QUOTE;
		
		/* process message */
		if(packetFromClient.type == BrokerPacket.BROKER_REQUEST) {
		    
		    String quote = nasdaq.get(packetFromClient.symbol);

		    if (quote == null){
			packetToClient.quote = Long.valueOf(0);
		    } else {
			packetToClient.quote = Long.parseLong(quote);
		    }
		    
		    /* send reply back to client */
		    toClient.writeObject(packetToClient);
		    
		    /* wait for next packet */
		    continue;
		}
		
		/*HANDLE EXCHANGE_ADD*/
		
		if(packetFromClient.type == BrokerPacket.EXCHANGE_ADD  ) {
			
			System.out.println("here1\n");
			String[] lineToArray = packetFromClient.symbol.split(" ");
			    
			nasdaq.put(lineToArray[1],"10");
			
				
			continue
		}
		
		
		System.out.println("lol"+packetFromClient.type);
		
		
		
		
		/* Sending an BROKER_NULL || BROKER_BYE means quit */
		if (packetFromClient.type == BrokerPacket.BROKER_NULL || packetFromClient.type == BrokerPacket.BROKER_BYE) {
		    gotByePacket = true;
		    packetToClient = new BrokerPacket();
		    packetToClient.type = BrokerPacket.BROKER_BYE;
		    toClient.writeObject(packetToClient);
		    break;
		}
		
		/* if code comes here, there is an error in the packet */
		System.err.println("ERROR: Unknown ECHO_* packet!!");
		System.exit(-1);
	    }
	    
	    /* cleanup when client exits */
	    fromClient.close();
	    toClient.close();
	    socket.close();
	    
	} catch (IOException e) {
	    if(!gotByePacket)
		e.printStackTrace();
	} catch (ClassNotFoundException e) {
	    if(!gotByePacket)
		e.printStackTrace();
	}
    }
}
