import java.net.*;
import java.io.*;
import java.util.*;

public class OnlineBroker {
    public static void main(String[] args) throws IOException{
	ServerSocket serverSocket = null;
	boolean listening = true;
	
	Map <String, String> nasdaq = new HashMap<String, String>();
	String thisLine = null;
	
        try {
	    if(args.length == 1) {
		serverSocket = new ServerSocket(Integer.parseInt(args[0]));
	    } else {
		System.err.println("ERROR: Invalid arguments!");
		System.exit(-1);
	    }
	    
	    /* do the table lookup */        	
	    try {
		BufferedReader br = new BufferedReader(new FileReader("nasdaq"));
		while ((thisLine = br.readLine()) != null){
		    String[] lineToArray = thisLine.split(" ");
		    nasdaq.put(lineToArray[0],lineToArray[1]);
		}
	    } catch (Exception e){
		e.printStackTrace();
		System.exit(-1);
	    }
        } catch (IOException e) {
            System.err.println("ERROR: Could not listen on port!");
            System.exit(-1);
        }
	
        while (listening) {
	    new BrokerServerHandlerThread(serverSocket.accept(), nasdaq).start();
        }
	
        serverSocket.close();
    }
}
