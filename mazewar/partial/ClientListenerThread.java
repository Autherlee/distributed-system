import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Hashtable;

public class ClientListenerThread implements Runnable {

    private MSocket mSocket  =  null;
    private Hashtable<String, Client> clientTable = null;
    private String name = "11";
	
   private Integer event_order = 0;
 private Hashtable<Integer,MPacket> event_table = new Hashtable<Integer,MPacket>();
    public ClientListenerThread( MSocket mSocket,
                                Hashtable<String, Client> clientTable){
        this.mSocket = mSocket;
        this.clientTable = clientTable;
        if(Debug.debug) System.out.println("Instatiating ClientListenerThread");
    }

    public void run() {
        MPacket received = null;
        Client client = null;
        if(Debug.debug) System.out.println("Starting ClientListenerThread");
        while(true){
            try{
			
                received = (MPacket) mSocket.readObject();
		
                System.out.println("Received " + received);

		Integer seq_num  = new Integer(received.sequenceNumber);

		 //System.out.println("Received seq_num:: " + seq_num);
		
		event_table.put(seq_num,received);
		
		received = event_table.get(event_order);

		
		
		
		while(received != null){        
		
		event_order ++;

		System.out.println("excute" +event_order);
	           
			if(received.event == MPacket.bullet){
				client = clientTable.get(name);
			}

			else{
		 		client = clientTable.get(received.name);
			}
                if(received.event == MPacket.UP){
                    client.forward();
                }else if(received.event == MPacket.DOWN){
                    client.backup();
                }else if(received.event == MPacket.LEFT){
                    client.turnLeft();
                }else if(received.event == MPacket.RIGHT){
                    client.turnRight();
                }else if(received.event == MPacket.FIRE){
                    client.fire();
                }
                else if(received.event == MPacket.bullet){
                	client.bullet_move();
                }
                else{
                    throw new UnsupportedOperationException();
                  
                } 
                received = event_table.get(event_order);
             
		}




		   
            }catch(IOException e){
                e.printStackTrace();
            }catch(ClassNotFoundException e){
                e.printStackTrace();
            }            
        }
    }
}
